﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Атрибут для отображения полей только когда игра не запущена.
 */


using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


// Позволяет атрибутам отображаться только когда игра не запущена
public class EditorOnlyAttribute : PropertyAttribute { }


#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(EditorOnlyAttribute))]
public class EditorOnlyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (!Application.isPlaying)
            return EditorGUI.GetPropertyHeight(property, label, true);

        return 0;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (!Application.isPlaying)
            EditorGUI.PropertyField(position, property, label, true);
    }
}

#endif
