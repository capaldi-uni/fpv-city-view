﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Контроллер симуляции города. Управляет генерацией и отображением 
 * объектов города, загрузкой и выгрузкой чанков, материалами и 
 * цветами объектов. Должен быть прикреплён к пустому объекту в сцене.
 */


using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

using GeoCoordinatePortable;

using MapObjects;
using CityGen;

using Extensions.CoordinateExtension;
using Extensions.NumberExtension;


// Управление загрузкой и выгрузкой чанков
public class CityController : MonoBehaviour
{
    [SerializeField]
    PlayerController Player;

    [SerializeField]
    Settings settings;

    public string DBPath => settings.DBPath;

    // Отображаение в редакторе
    [Header("Debug")][SerializeField][ReadOnly]
    private string Origin;

    // Время до следующего обновления чанков
    [SerializeField][ReadOnly]
    private float updateTimer = 0;


    [Header("Materials")]
    [SerializeField]
    private Material BuildingMaterial;

    [SerializeField]
    private Material RoofMaterial;

    [SerializeField]
    private Material GroundMaterial;

    [SerializeField]
    private Color EmptyColor;


    private CityGenerator generator;

    // Точка отсчёта города
    private (double lat, double lon) _origin;

    private double MeterScaleLat;
    private double MeterScaleLon;

    // Для отладки
    public (double lat, double lon) origin 
    { 
        get => _origin; 
        set
        {
            _origin = value;
            Origin = _origin.ToString();
        } 
    }

    // Чанк, в котором находится игрок
    private (double lat, double lon) playerChunk;
    private (double lat, double lon) playerLastChunk;

    // Список активных чанков
    private List<(double lat, double lon)> active;

    // Обновление уже идёт
    private bool isUpdating = false;

    void Start()
    {
        if (!PersistentState.Contains("Origin"))
        {
            SceneManager.LoadScene("Scenes/MapMenuScene");
            return;
        }

        active = new List<(double lat, double lon)>();

        // Наполнение БД при отсутствии
        if (!File.Exists(settings.DBPath))
        {
            Debug.LogError("No database found! Please load menu scene first!");
            return;
        }

        using (var db = new MapObjectsDB(settings.DBPath))
        {
            if (!db.Chunks.Exists(_ => true))
                Debug.Log("Database is empty");
        }
        
        // Получение начального чанка
        var desiredPoint = PersistentState.Get<(double, double)>("Origin", (0, 0));

        origin = ToChunkCoordinates(desiredPoint);
        Player.transform.position = GetUnityPosition(desiredPoint);


        // Создание генератора
        generator = new CityGenerator(settings)
        {
            BuildingMaterial = BuildingMaterial,
            RoofMaterial = RoofMaterial,
            GroundMaterial = GroundMaterial,
            EmptyColor = EmptyColor,
        };

        // Вычисляем цену метра вдоль координатных осей
        CalculateMeterScale();

        // Игрок находится в начальном чанке
        playerChunk = origin;
        playerLastChunk = playerChunk;

        // Генерим начальную область города за один заход
        CompleteCoroutine(UpdateChunks());
    }

    private void FixedUpdate()
    {
        CalculatePlayerChunk();
    }

    void Update()
    {
        updateTimer += Time.deltaTime;

        // Обновляем чанки каждые три секунды или при смене чанка
        if (!isUpdating && (updateTimer > 3 || playerChunk != playerLastChunk))
        {
            updateTimer = 0;
            playerLastChunk = playerChunk;

            StartCoroutine(UpdateChunks());
        }
    }

    // Обновление чанков - генерация новых, выключение ненужных
    private IEnumerator UpdateChunks()
    {
        isUpdating = true;

        var v = Player.ViewDistance;

        // Списко чанков, активных после этого апдейта
        var newActive = new List<(double lat, double lon)>();
        
        // Получаем координаты чанков в области видимости игрока
        for (int i = -v; i <= v; i++)
        {
            for (int j = -v; j <= v; j++)
            {
                // Округление, чтобы избежать ошибок разряда 0.1 + 0.2 != 0.3
                var c = (Math.Round(playerChunk.lat + i * settings.ChunkSize, settings.Precision), 
                         Math.Round(playerChunk.lon + j * settings.ChunkSize, settings.Precision));

                newActive.Add(c);
            }
        }

        // Перерыв до следующего кадра
        yield return null;

        // Выключаем чанки, которые стали неактивны
        foreach (var chunk in active.Except(newActive))
        {
            var obj = generator.GetChunkAt(chunk.lat, chunk.lon);

            if (obj != null)
                obj.SetActive(false);
        }

        // Перерыв до следующего кадра
        yield return null;

        // Генерация новых чанков - создание корутины
        var chunkGen = GenerateChunks(newActive.Except(active).ToList());

        // Пошаговое выполнение корутины
        // Тут бы не помешал yield from из питона
        while (chunkGen.MoveNext())
        {
            yield return null;
        }

        // Заменяем список активных чанков
        active = newActive;
        isUpdating = false;
    }

    // Генерация чанков
    private IEnumerator GenerateChunks(List<(double lat, double lon)> toGenerate)
    {
        foreach (var chunk in toGenerate)
        {
            // Получаем чанк от генертора
            var obj = generator.GetChunkAt(chunk.lat, chunk.lon);

            // Осталось с прошлой версии генератора, пока оставляю в качестве предосторожности
            if (obj == null)
            {
                Debug.LogWarning("Got null in GenerateChunks");
                active.Remove(chunk);
                continue;
            }

            // Если чанк активен, значит новый
            if (obj.activeSelf)
            {
                // Втыкаем в город
                obj.transform.parent = transform;

                // И назначаем позицию
                obj.transform.localPosition = generator.GetChunkPosition(obj, new GeoCoordinate(origin.lat, origin.lon));
            }
            else  // Старый чанк просто активируем
                obj.SetActive(true);

            // По одному чанку за раз
            yield return null;
        }
    }

    // Вычисляет цену метра для широты и долготы
    private void CalculateMeterScale()
    {
        var zero = new GeoCoordinate(origin.lat, origin.lon);

        MeterScaleLat = 1 / zero.GetDistanceTo(zero.Add(latitude: 1));
        MeterScaleLon = 1 / zero.GetDistanceTo(zero.Add(longitude: 1));
    }

    // Определяет позицию игрока в географических координатах
    private void CalculatePlayerChunk()
    {
        var pos = GetPlayerGeoPosition();

        playerChunk = ToChunkCoordinates(pos.lat, pos.lon);
    }

    public (double lat, double lon) GetPlayerGeoPosition()
    {
        return (
            Player.transform.position.z * MeterScaleLat + origin.lat, 
            Player.transform.position.x * MeterScaleLon + origin.lon
        );
    }

    // Преобразование гео-координат в координаты сцены
    public Vector3 GetUnityPosition((double lat, double lon) c)
    {
        var zero = new GeoCoordinate(origin.lat, origin.lon);

        var z = zero.GetDistanceTo(new GeoCoordinate(c.lat, zero.Longitude));
        var x = zero.GetDistanceTo(new GeoCoordinate(zero.Latitude, c.lon));

        return new Vector3((float)x, 0, (float)z);
    }

    // Определяет чанк в котором находится объект
    private (double lat, double lon) ToChunkCoordinates(double lat, double lon)
    {
       return (lat.Whole(settings.ChunkSize), lon.Whole(settings.ChunkSize));
    }

    private (double lat, double lon) ToChunkCoordinates((double lat, double lon) c)
    {
        return ToChunkCoordinates(c.lat, c.lon);
    }

    // Выполняет корутину полностью за один вызов
    public static void CompleteCoroutine(IEnumerator coro)
    {
        while (coro.MoveNext()) { }
    }

    public void ReloadMaterials()
    {
        generator.BuildingMaterial = BuildingMaterial;
        generator.RoofMaterial = RoofMaterial;
        generator.GroundMaterial = GroundMaterial;
        generator.EmptyColor = EmptyColor;

        StartCoroutine(generator.ReloadMaterials());
    }
}
