﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Контроллер главного меню программы. Управляет вводом координат и
 * процессами загрузки и обработки данных. Отображает сообщения об
 * ошибках пользователю.
 */


using System;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Text.RegularExpressions;

using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

using MapObjects;

using Coro;


public class MenuController : MonoBehaviour
{
    [SerializeField]
    private Settings settings;

    [SerializeField]
    private TMP_InputField inputField;

    [SerializeField]
    private string placeholderText;

    [SerializeField]
    private GameObject inputForm;

    [SerializeField]
    private GameObject loadingForm;

    [SerializeField]
    private GameObject errorForm;

    [SerializeField]
    private TextMeshProUGUI errorText;

    // Регулярное выражение для строки координат
    private Regex regex = new(@"^\s*(?<lat>-?\d+([.,]\d+)?)\s*°?\s*(?<latMark>N|n|S|s|)\s*,?\s+(?<lon>-?\d+([.,]\d+)?)\s*°?\s*(?<lonMark>E|e|W|w|)\s*$");
    private Regex regex2 = new(@"^(?<latDeg>-?\d{1,2})(°|\s)\s*(?<latMin>\d{1,2})('|\s)\s*(?<latSec>\d{1,2}((\.|,)\d+)?)(""|\s|)\s*(N|n|S|s)?((,\s*)|\s+)(?<lonDeg>-?\d{1,2})(°|\s)\s*(?<lonMin>\d{1,2})('|\s)\s*(?<lonSec>\d{1,2}((\.|,)\d+)?)(""|\s|)\s*(E|e|W|w)?$");

    private Coro.Coroutine _cr;  // Текущая корутина

    void Start()
    {
        // Устанавливаем инвариантную культуру чтобы double воспринимался с `.` вместо `,`
        CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
        CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

        Cursor.lockState = CursorLockMode.None;

        inputField.placeholder.GetComponent<TextMeshProUGUI>().text = placeholderText;

        OpenInputForm();
    }

    // Считать координату, проверить и передать сцене города
    public void OnStartPressed()
    {
        string text = placeholderText;

        if (inputField.text.Length > 0)
            text = inputField.text;

        (double lat, double lon) coord;

        try
        {
            coord = ParseCoordinates(text);
        }
        catch (ArgumentException ex)
        {
            Debug.Log(ex.Message);

            ShowError(ex.Message);

            return;
        }

        PersistentState.Set("Origin", coord);

        StartCoroutine(LoadCity(coord));
    }

    // Загрузка данных карты
    private IEnumerator LoadCity((double lat, double lon) c)
    {
        OpenLoadingForm();

        bool exists = false;

        // Проверка необходимости загрузки
        if (!File.Exists(settings.DBPath))
        {
            Debug.Log("No database. Creating");
        }
        else
        {
            using (var db = new MapObjectsDB(settings.DBPath))
            {
                if (db.Fragments.Exists(Fragment.Includes(c)))
                {
                    exists = true;
                }
            }
        }

        yield return null;

        if (!exists)
        {
            MapDownloader downloader = new MapDownloader(settings);

            yield return _cr = downloader.getMap(c).Coroutine();
            if (ReportException(_cr)) yield break;
            
            
            if (!downloader.done || downloader.error)
            {
                Debug.Log("Cannot export map");

                ShowError("Error while exporting map");

                yield break;
            }

            MapObjectsParser parser = new MapObjectsParser(settings);
            
            yield return _cr = parser.Parse(downloader.filename).Coroutine();
            if (ReportException(_cr)) yield break;

            yield return _cr = parser.fillMissingValues().Coroutine();
            if (ReportException(_cr)) yield break;
        }

        SceneManager.LoadScene("Scenes/CityScene");
    }

    // Парсинг гео-координаты
    private (double lat, double lon) ParseCoordinates(string text)
    {
        double lat, lon;

        var match = regex.Match(text);

        if (match.Success)
        {
            lat = double.Parse(match.Groups["lat"].Value.Replace(',', '.'));
            lon = double.Parse(match.Groups["lon"].Value.Replace(',', '.'));
        }
        else
        {
            match = regex2.Match(text);

            if (match.Success)
            {
                double deg, min, sec;

                deg = double.Parse(match.Groups["latDeg"].Value);
                min = double.Parse(match.Groups["latMin"].Value);
                sec = double.Parse(match.Groups["latSec"].Value.Replace(',', '.'));

                lat = deg + (min + sec / 60) / 60;

                deg = double.Parse(match.Groups["lonDeg"].Value);
                min = double.Parse(match.Groups["lonMin"].Value);
                sec = double.Parse(match.Groups["lonSec"].Value.Replace(',', '.'));

                lon = deg + (min + sec / 60) / 60;

            }
            else 
                throw new ArgumentException("Invalid coordinates provided. Please try again.");
        }
            
        if (lat < -90 || lat > 90 || lon < -180 || lon > 180)
            throw new ArgumentException("Provided coordinates are out of range.");

        if (match.Groups["latMark"].Value.ToUpper() == "S")
            lat = -lat;

        if (match.Groups["lonMark"].Value.ToUpper() == "W")
            lon = -lon;

        return (lat, lon);
    }

    // Переключение активности экранов
    private void OpenInputForm()
    {
        inputForm.SetActive(true);
        loadingForm.SetActive(false);
        errorForm.SetActive(false);
    }

    private void OpenLoadingForm()
    {
        loadingForm.SetActive(true);
        inputForm.SetActive(false);
        errorForm.SetActive(false);
    }

    private void OpenErrorForm()
    {
        errorForm.SetActive(true);
        inputForm.SetActive(false);
        loadingForm.SetActive(false);
    }

    private void ShowError(string message)
    {
        errorText.text = message;
        OpenErrorForm();
    }

    // TODO: сообщить о проблеме пользователю
    private bool ReportException(Coro.Coroutine routine)
    {
        if (!routine.Finished)
        {
            Debug.LogWarning("Coroutien not finished. This should not happen!");
        }

        if (routine.CurrentException != null)
        {
            Debug.LogException(routine.CurrentException);

            ShowError(routine.CurrentException.Message);

            return true;
        }

        return false;
    }

    public void OnOK()
    {
        OpenInputForm();
    }

    public void Exit()
    {
        Application.Quit();
    }
}
