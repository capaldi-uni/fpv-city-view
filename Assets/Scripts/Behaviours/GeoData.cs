﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Компонент дополнительной информации. Позволяет хранить дополнительную 
 * информацию, привязанную к объекту симуляции.
 */


using UnityEngine;

using GeoCoordinatePortable;


// Компонент для объектов города, хранящий географические и отладочные данные
public class GeoData : MonoBehaviour
{
    public double Lat;

    public double Lon;

    public GeoCoordinate Coordinate
    {
        get { return new GeoCoordinate(Lat, Lon); }
        set
        {
            Lat = value.Latitude;
            Lon = value.Longitude;
        }
    }

    // Словарь отладочных тэгов
    public TagDict Tags = new TagDict();
}
