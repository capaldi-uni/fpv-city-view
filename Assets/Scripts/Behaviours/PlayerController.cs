﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Контроллер игрока/аватара пользователя. Обеспечивает управление
 * аватаром в пространстве модели.
 */


using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Transform Capsule;

    [SerializeField]
    private Transform Camera;

    [Header("Settings")]
    [SerializeField][EditorOnly][Expandable]
    private PlayerSettings playerSettings;

    [SerializeField][RuntimeOnly]
    private float accForce;

    [SerializeField][RuntimeOnly]
    private float jetForce;

    [SerializeField][RuntimeOnly]
    private float maxWalkSpeed;

    [SerializeField][RuntimeOnly]
    private float maxSpringSpeed;

    [SerializeField][RuntimeOnly][Range(0.1f, 10)]
    private float mouseSensitivity;

    [SerializeField][RuntimeOnly][Range(1, 7)]
    private int viewDistance;

    public int ViewDistance => viewDistance;

    [Header("Debug")]
    [SerializeField][ReadOnly]
    private bool grounded = false;

    [SerializeField][ReadOnly]
    private bool hovering = false;

    [Header("Events")]

    // Сообщение, что был запрошен экран отладки
    [SerializeField]
    UnityEvent DebugScreenToggled;

    // Сообщение, что было запрошено копирование координат
    [SerializeField]
    UnityEvent<bool> CoordinateCopyRequested;

    // Состояние элеменнтов ввода
    private Vector2 move = Vector2.zero;    // Направление движения
    private bool sprint = false;    // Спринт или нет
    private bool jet = false;   // Активен ли реактивный ранец

    // Текущий угол камеры
    // Так как unity работает с углами через кватернионы, конверсия в эйлеровы углы может быть неоднозначной
    // Поэтому я отдельно слежу за этим углом, чтобы я мог полностью его контролировать
    private float camAngle = 0;

    private Rigidbody rb;

    void Awake()
    {
        accForce = playerSettings.AccForce;
        jetForce = playerSettings.JetForce;

        maxWalkSpeed = playerSettings.MaxWalkSpeed;
        maxSpringSpeed = playerSettings.MaxSprintSpeed;

        mouseSensitivity = playerSettings.MouseSensitivity;
        viewDistance = playerSettings.ViewDistance;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // Проверить нахождение на земле
        grounded = Physics.Raycast(Capsule.position, -transform.up, 2);

        moveForce();
        jetPack();
    }

    void moveForce()
    {
        float maxSpeed = sprint ? maxSpringSpeed : maxWalkSpeed;
        
        if (grounded && rb.velocity.magnitude > maxSpeed) return;

        float force = (hovering || !grounded) ? jetForce : accForce;

        rb.AddForce(transform.rotation * new Vector3(move.x, 0, move.y).normalized * force * 10);
    }

    void jetPack()
    {
        if (jet)
            rb.AddForce(10 * jetForce * transform.up);

        else if (!grounded && sprint)
            rb.AddForce(10 * jetForce * -transform.up);
    }

    void OnApplicationFocus(bool focus)
    {
        if (focus)
            Cursor.lockState = CursorLockMode.Locked;
    }

    void OnMove(InputValue value)
    {
        // Регистрация движения
        move = value.Get<Vector2>();        
    }

    void OnLook(InputValue value)
    {
        Vector2 look = value.Get<Vector2>();

        // Горизонтально вращается сам игрок
        transform.Rotate(new Vector3(0, look.x, 0) * mouseSensitivity);

        // Вертикально вращается "голова" - камера. 
        //Так как нам не желательно вращение 360° (нужно только передние 180°), ограничиваем итоговый угол
        camAngle = ClampCamAngle(camAngle + -look.y * mouseSensitivity);
        Camera.localRotation = Quaternion.Euler(camAngle, 0, 0);
    }

    void OnRun(InputValue value)
    {
        // Регистрируем бег
        sprint = value.Get<float>() > 0;
    }

    void OnJet(InputValue value)
    {
        jet = value.Get<float>() > 0;
    }

    void OnHover(InputValue value)
    {
        hovering = !hovering;
        rb.useGravity = !hovering;
    }

    void OnToggleDebugScreen()
    {
        DebugScreenToggled.Invoke();
    }

    void OnOpenMenu()
    {
        SceneManager.LoadScene("Scenes/MapMenuScene");
    }

    void OnCopyCoordinate()
    {
        CoordinateCopyRequested.Invoke(false);
    }

    void OnCopyCoordinateAlt()
    {
        CoordinateCopyRequested.Invoke(true);
    }

    void OnFire()
    {
        OnApplicationFocus(true);
    }

    // Ограничиваем угол поворота камеры по вертикали
    private float ClampCamAngle(float angle)
    {
        return Mathf.Clamp(angle, -90, 90);
    }
}
