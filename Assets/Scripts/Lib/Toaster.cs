using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

// https://stackoverflow.com/a/52592601/9646065
// ������������� ������������� ����� ��������������� ������������ �����������
public class Toaster : MonoBehaviour
{
    public TextMeshProUGUI toast;

    void Start()
    {
        toast.enabled = false;
    }

    public void ShowToast(string text, int duration)
    {
        StartCoroutine(ShowToastCOR(text, duration));
    }

    private IEnumerator ShowToastCOR(string text, int duration)
    {
        Color orginalColor = toast.color;

        toast.text = text;
        toast.enabled = true;

        //Fade in
        yield return fadeInAndOut(toast, true, 0.5f);

        //Wait for the duration
        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            yield return null;
        }

        //Fade out
        yield return fadeInAndOut(toast, false, 0.5f);

        toast.enabled = false;
        toast.color = orginalColor;
    }

    IEnumerator fadeInAndOut(TextMeshProUGUI targetText, bool fadeIn, float duration)
    {
        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn)
        {
            a = 0f;
            b = 1f;
        }
        else
        {
            a = 1f;
            b = 0f;
        }

        Color currentColor = targetText.color;
        float counter = 0f;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            targetText.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
            yield return null;
        }
    }
}
