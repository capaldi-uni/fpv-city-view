﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Загрузчик данных при помощи API OpenStreetMap.
 */


using System.Collections;

using System.IO;

using UnityEngine;
using UnityEngine.Networking;


namespace MapObjects
{
    public class MapDownloader
    {
        private static string baseUrl = "https://api.openstreetmap.org/api/0.6/map?bbox={0},{1},{2},{3}";

        private Settings settings;

        public string filename { get; private set; } = null; // Путь к загруженному файлу
        public bool done { get; private set; } = false;  // Загрузка завершена
        public bool error { get; private set; } = false; // Произошла ошибка

        public MapDownloader(Settings settings)
        {
            this.settings = settings;
        }

        // Сформировать запрос к API
        private static string makeUrl((double minLon, double minLat, double maxLon, double maxLat) BBox)
        {
            return string.Format(baseUrl, BBox.minLon, BBox.minLat, BBox.maxLon, BBox.maxLat);
        }

        // Загружает фрагмент карты с центром в заданной точке
        public IEnumerator getMap((double lat, double lon) coord)
        {
            return getMap(settings.AsBBox(coord));
        }

        // Загружает фрагмент карты ограниченный заданным прямоугольником
        public IEnumerator getMap((double minLon, double minLat, double maxLon, double maxLat) BBox)
        {
            Debug.Log($"Downloading map for {BBox}");

            done = false;
            error = false;

            var url = makeUrl(BBox);
            filename = Path.Combine(settings.MapFolder, "tmp.osm");

            Debug.Log($"Saving to \"{filename}\"");

            if (!Directory.Exists(Path.GetDirectoryName(filename)))
                Directory.CreateDirectory(Path.GetDirectoryName(filename));

            // Хэндлер для сохранения результата в файл
            var downloadHandler = new DownloadHandlerFile(filename)
            {
                removeFileOnAbort = true
            };

            var request = new UnityWebRequest(url) 
            { 
                method = UnityWebRequest.kHttpVerbGET,
                downloadHandler = downloadHandler
            };

            yield return request.SendWebRequest();

            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.Log(request.error);
                    Debug.Log("Url was: " + url);
                    error = true;
                    break;

                case UnityWebRequest.Result.Success:
                    Debug.Log("Download successful");
                    break;

                default:
                    Debug.Log("Something went wrong");
                    break;
            }

            done = true;
        }
    }
}