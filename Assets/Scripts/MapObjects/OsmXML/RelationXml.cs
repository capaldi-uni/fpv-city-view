﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Отношение, группирующее другие элементы карты.
 */


using System.Xml.Serialization;
using System.Collections.Generic;


namespace MapObjects.OsmXML
{
    // Отношение, как-то группирующее элементы
    [XmlRoot(ElementName = "relation")]
    public class RelationXml : BaseElementXml
    {
        [XmlElement(ElementName = "member")]
        public List<MemberXml> Member { get; set; }
    }
}
