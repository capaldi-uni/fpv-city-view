﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Путь - последовательность узлов, представляющая сложные объекты на карте.
 */


using System.Xml.Serialization;
using System.Collections.Generic;

namespace MapObjects.OsmXML
{
    // Путь - последовательность узлов. Здания представляются путями
    [XmlRoot(ElementName = "way")]
    public class WayXml : BaseElementXml
    {
        [XmlElement(ElementName = "nd")]
        public List<NdXml> Nodes { get; set; }
    }
}
