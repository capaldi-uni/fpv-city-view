﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Тэг объекта на карте.
 */


using System.Xml.Serialization;


namespace MapObjects.OsmXML
{
    // Тэг элемента
    [XmlRoot(ElementName = "tag")]
    public class TagXml
    {
        [XmlAttribute(AttributeName = "k")]
        public string K { get; set; }


        [XmlAttribute(AttributeName = "v")]
        public string V { get; set; }
    }
}
