﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Базовый элемент для большинства элементов в XML-файле карты.
 */


using System.Xml.Serialization;
using System.Collections.Generic;


namespace MapObjects.OsmXML
{
    // Общие аттрибуты и элементы для большинства остальных элементов
    public class BaseElementXml
    {
        [XmlElement(ElementName = "tag")]
        public List<TagXml> Tags { get; set; }


        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }


        [XmlAttribute(AttributeName = "visible")]
        public string Visible { get; set; }


        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }


        [XmlAttribute(AttributeName = "changeset")]
        public string Changeset { get; set; }


        [XmlAttribute(AttributeName = "timestamp")]
        public string Timestamp { get; set; }


        [XmlAttribute(AttributeName = "user")]
        public string User { get; set; }


        [XmlAttribute(AttributeName = "uid")]
        public string Uid { get; set; }
    }
}