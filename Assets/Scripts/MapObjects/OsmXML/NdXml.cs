﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Ссылка на узел.
 */


using System.Xml.Serialization;


namespace MapObjects.OsmXML
{
    // Ссылка на узел/вершину
    [XmlRoot(ElementName = "nd")]
    public class NdXml
    {
        [XmlAttribute(AttributeName = "ref")]
        public string Ref { get; set; }
    }
}
