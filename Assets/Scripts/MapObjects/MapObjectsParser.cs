﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Парсер файлов карты для заполненяи БД.
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using System.Globalization;

using UnityEngine;

using LiteDB;

using MapObjects.OsmXML;


namespace MapObjects
{
    // Парсер xml файлов карты для заполнения бд
    public class MapObjectsParser
    {
        private MapObjectsDB db;  // База данных

        private OsmXml osm;  // Десериализованный XML-файл карты 
        private IEnumerable<WayXml> buildingWays;  // Пути, описывающие здания
        private List<Node> nodes;   // Добавленные узлы

        private List<Chunk> fragmentChunks;  // Чанки текущего фрагмента

        private Fragment fragment;  // Текущий фрагмент

        private Settings settings;  // Настройки

        public MapObjectsParser(Settings settings)
        {
            this.settings = settings;
        }

        // Распарсить xml файл карты и заполнить бд
        public IEnumerator Parse(string filename, bool update = false)
        {
            Debug.Log("Starting parsing");

            // Десериализация xml файла
            osm = OsmXml.fromFile(filename);

            // Подключение к бд
            using (db = new MapObjectsDB(settings.DBPath))
            { 
                BoundsXml bounds = osm.Bounds;

                (double minLon, double minLat, double maxLon, double maxLat) bbox = 
                    (double.Parse(bounds.Minlon, CultureInfo.InvariantCulture),
                     double.Parse(bounds.Minlat, CultureInfo.InvariantCulture), 
                     double.Parse(bounds.Maxlon, CultureInfo.InvariantCulture), 
                     double.Parse(bounds.Maxlat, CultureInfo.InvariantCulture));

                if (!update && db.Fragments.Exists(Fragment.WithBBox(bbox)))
                    yield break;

                Debug.Log("Selecting buildings");

                // Выборка задний
                buildingWays = osm.Ways.Where(x => x.Tags.Any(y => y.K == "building" || y.K == "building:part"));

                // Список уже добавленных узлов
                nodes = new List<Node>();

                // Список чанков
                fragmentChunks = new List<Chunk>();
            
                // Начать транзацию
                if (!db.BeginTrans())
                    throw new InvalidOperationException("Already in transaction!");

                Debug.Log("Processing buildings");

                // Обход списка зданий и создание объектов зданий в бд
                foreach (WayXml w in buildingWays)
                {
                    // Парсинг id из строки
                    long id = long.Parse(w.Id);

                    // Найти здание в бд
                    Building building = db.Buildings.FindOne(b => b.Id == id);

                    // Создать здание, если не существует
                    if (building == null)
                        building = buildBuilding(w);

                    // Определение чанка здания | также чанк добавляется в список чанков фрагмента
                    Chunk chunk = getChunkForBuilding(building);

                    // Добавить здание чанку, если не добавлено ранее
                    if (!chunk.Buildings.Contains(building))
                        chunk.Buildings.Add(building);

                    yield return null;
                }

                Debug.Log("Adding chunks");

                // Добавление всех чанков в базу данных
                foreach (Chunk c in fragmentChunks)
                {
                    if (db.Chunks.Exists(Chunk.EqualTo(c)))
                        db.Chunks.Update(c);
                    else
                        db.Chunks.Insert(c);
                }

                Debug.Log("Creating fragment");

                // Найти текущий фрагмент в БД, если есть
                fragment = db.Fragments.FindOne(Fragment.WithBBox(bbox));

                if (fragment == null)
                {
                    // Создать фрагмент
                    fragment = new Fragment
                    {
                        MinLon = bbox.minLon,
                        MinLat = bbox.minLat,
                        MaxLon = bbox.maxLon,
                        MaxLat = bbox.maxLat,

                        // Chunks = fragmentChunks
                    };

                    // Добавление фрагмента в БД
                    db.Fragments.Insert(fragment);
                }   
                else
                {
                    // fragment.Chunks = fragment.Chunks.Union(fragmentChunks).ToList();
                    db.Fragments.Update(fragment);
                }

                Debug.Log("Saving");

                db.Commit();
            }

            Debug.Log("Finished parsing");
        }

        // Заполнение прпопущеных значений
        public IEnumerator fillMissingValues()
        {
            Debug.Log("Filling missing values");

            using (db = new MapObjectsDB(settings.DBPath))
            {
                if (!db.BeginTrans())
                    throw new InvalidOperationException("Already in transaction!");

                // Все здания с пропущенным значением
                var buildings = db.Buildings.Find(b => b.Levels == 0).ToList();

                foreach (var b in buildings)
                {
                    // Все ближайшие здания
                    var nearby = db.Buildings.Find(
                        Building.Nearby(b, settings.NearbyRadius)
                    ).Select(bb => bb.Levels).Where(lvl => lvl > 0);

                    if (nearby.Any())
                        b.Levels = (int)nearby.Average();

                    if (b.Levels == 0)
                        b.Levels = settings.DefaultLevels;

                    db.Buildings.Update(b);

                    yield return null;
                }

                Debug.Log("Saving");

                db.Commit();
            }

            Debug.Log("Finished filling missing values");
        }

        // Получить чанк в котором должно находиться здание
        private Chunk getChunkForBuilding(Building building)
        {
            // Определяем координаты чанка здания
            double lat = building.Lat - (building.Lat % settings.ChunkSize);
            double lon = building.Lon - (building.Lon % settings.ChunkSize);

            Chunk chunk = null;

            // Находим чанк в списке уже созданных, если есть
            chunk = fragmentChunks.FirstOrDefault(ch => ch.Lat == lat && ch.Lon == lon);

            if (chunk != null)
                return chunk;

            // Найти чанк в бд, если он там есть
            chunk = db.Chunks.FindOne(Chunk.LocatedAt((lat, lon), settings));

            if (chunk != null)
            {
                fragmentChunks.Add(chunk);
                return chunk;
            }

            // Создаём чанк при отсутствии
            chunk = new Chunk { Lat = lat, Lon = lon, Buildings = new List<Building>() };
            fragmentChunks.Add(chunk);

            return chunk;
        }

        // Получить существующий или новый узел по id
        private Node getNodeById(string id)
        {
            // Парсинг id из строки
            long longid = long.Parse(id);

            Node node = null;

            // Попытаться найти узел с данным id в добавленных
            node = nodes.FirstOrDefault(x => x.Id == longid);

            if (node != null)
                return node;

            // Найти узел в бд
            node = db.Nodes.FindOne(nd => nd.Id == longid);

            if (node != null)
            {
                nodes.Add(node);
                return node;
            }

            // Если не найдено
            
            // Найти узел в распарсеном xml файле
            NodeXml n = osm.Nodes.First(x => x.Id == id);

            // Создать объект на его основе
            node = new Node
            {
                Id = longid,
                Lat = double.Parse(n.Lat, CultureInfo.InvariantCulture),
                Lon = double.Parse(n.Lon, CultureInfo.InvariantCulture)
            };

            // Добавить в коллекцию и в список добавленных узлов
            db.Nodes.Insert(node);
            nodes.Add(node);

            // Вернуть новый узел
            return node;
        }

        private Building buildBuilding(WayXml way)
        {
            // Парсинг id из строки
            long id = long.Parse(way.Id);

            // Список узлов текущего здания
            List<Node> curentNodes = new List<Node>();

            // Парсинг узлов
            foreach (NdXml nd in way.Nodes)
            {
                Node node = getNodeById(nd.Ref);

                // Добавление узла в список
                curentNodes.Add(node);
            }

            // Получение количества этажей
            var levelStr = way.Tags.Where(x => x.K == "building:levels").FirstOrDefault()?.V ?? "0";

            var res = levelStr.Split(new char[] { ';' }, 2);

            int levels = 0;

            try
            {
                levels = (int)double.Parse(res[0], CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                Debug.Log($"Strange levels value: {levelStr}");
            }

            if (levels == 0)
            {
                var heightStr = way.Tags.Where(x => x.K == "height").FirstOrDefault()?.V ?? "0";

                try
                {
                    var height = double.Parse(heightStr, CultureInfo.InvariantCulture);
                    levels = (int)(height / settings.LevelHeight);
                }
                catch (FormatException)
                {
                    Debug.Log($"Strange height value: {heightStr}");
                }
            }

            // Создание объекта здания
            Building building = new Building
            {
                Id = id,
                Nodes = curentNodes,
                Levels = levels,
                Lat = curentNodes.Aggregate((prev, next) => prev.Lat < next.Lat ? prev : next).Lat,
                Lon = curentNodes.Aggregate((prev, next) => prev.Lon < next.Lon ? prev : next).Lon
            };

            // Добавление в бд
            db.Buildings.Insert(building);

            return building;
        }
    }
}
