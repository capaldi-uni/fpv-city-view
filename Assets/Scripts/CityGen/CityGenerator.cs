﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Генератор города. Создаёт объекты зданий и чанков на основе данных
 * из предоставленной БД.
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using GeoCoordinatePortable;

using MapObjects;
using Geometry;

using Extensions.CoordinateExtension;
using Extensions.IterableExtension;


namespace CityGen
{
    // Генератор объектов города на основе данных из БД
    public class CityGenerator
    {
        // Массив  загруженных чанков
        private Dictionary<(double, double), GameObject> LoadedChunks;

        private Settings settings;

        // Материалы зданий и поверхности
        public Material BuildingMaterial { get; set; } = null;
        public Material RoofMaterial { get; set; } = null;
        public Material GroundMaterial { get; set; } = null;

        public Color EmptyColor { get; set; } = new Color();

        public CityGenerator(Settings settings)
        {
            this.settings = settings;

            LoadedChunks = new Dictionary<(double, double), GameObject>();
        }

        // Получение чанка на заданных координатах
        public GameObject GetChunkAt(double lat, double lon)
        {
            if (!LoadedChunks.ContainsKey((lat, lon)))
                LoadChunkAt(lat, lon);

            return LoadedChunks[(lat, lon)];
        }

        // Загрузка из базы данных чанка с заданными координтами и создание соответствующего объекта
        private void LoadChunkAt(double lat, double lon)
        {
            using (var db = new MapObjectsDB(settings.DBPath))
            {
                // Получить чанк с заданными широтой и долготой
                Chunk chunk = db.QueryChunks.Where(Chunk.LocatedAt((lat, lon), settings)).FirstOrDefault();

                if (chunk == null)  // Чанк отсутствует в БД
                {
                    // Создаём пустой
                    LoadedChunks.Add((lat, lon), CreateEmptyChunkObject(lat, lon));
                    return;
                }

                // Создать объект чанка
                LoadedChunks.Add((lat, lon), CreateChunkObject(chunk));
            }
        }

        // Создание объекта чанка на основе записи из БД
        private GameObject CreateChunkObject (Chunk chunk)
        {
            // Объект чанка
            GameObject obj = new GameObject($"Chunk[{chunk.Lat}, {chunk.Lon}]", typeof(GeoData));

            // Добавляем координаты в гео данные
            obj.GetComponent<GeoData>().Coordinate = chunk.Coordinate();

            // Отладочный тэг
            obj.GetComponent<GeoData>().Tags["Chunk"] = chunk.ToString(); 

            // Обход списка зданий и создани объектов для них
            foreach (var building in chunk.Buildings)   
            {
                GameObject buildingObject = CreateBuildingObject(building);

                // Делаем здание дочерним элементом чанка
                buildingObject.transform.parent = obj.transform;

                // Назначаем позицию в соответствии с координатами
                buildingObject.transform.localPosition = GetUnityPosition(building.Coordinate(), chunk.Coordinate());
            }

            // Создание поверхности земли
            GameObject floor = CreateChunkFloor(chunk.Lat, chunk.Lon);
            floor.transform.parent = obj.transform;
            floor.transform.localPosition = Vector3.zero;

            return obj;
        }

        // Создаёт пустой чанк по заданным координатам
        // Координаты нужны чтобы пол был более-менее правильного размера
        private GameObject CreateEmptyChunkObject(double lat, double lon)
        {
            // Объект чанка
            GameObject obj = new GameObject($"Chunk[{lat}, {lon}](Empty)", typeof(GeoData));

            // Координаты в гео-данные и тэги
            obj.GetComponent<GeoData>().Coordinate = new GeoCoordinate(lat, lon);
            obj.GetComponent<GeoData>().Tags["Chunk"] = $"Chunk[at ({lat}, {lon}), empty]";

            // Создание поверхности земли
            GameObject floor = CreateChunkFloor(lat, lon);
            floor.transform.parent = obj.transform;
            floor.transform.localPosition = Vector3.zero;

            floor.GetComponent<MeshRenderer>().material.color = EmptyColor;

            return obj;
        }

        // Создание объекта здания на основе записи из БД
        private GameObject CreateBuildingObject(Building building)
        {
            float uvScale = (float)(1 / settings.LevelHeight);

            // Точка отсчёта - юго-западный угол описывающего прямоугольника здания
            GeoCoordinate origin = building.Coordinate(); 

            // Список вершин здания, преобразованных в векторы unity (с вычисленной позицией в пространстве unity)
            List<Vector3> initialVertices = building.Nodes.Select(n => GetUnityPosition(n.Coordinate(), origin)).ToList();

            // Убеждаемся, что порядок обхода вершин - против часовой стрелки
            //  Так как первая вершина совпадает с последней (дублируется), пропускаем её, чтобы не учитывать дважды
            if (GeometryFunctions.IsClockwise(initialVertices.SkipLast().ToList()))
                initialVertices.Reverse();

            // Массив вершин, треугольников и UV-координат будущего меша
            List<Vector3> vertices = new();
            List<int> triangles = new();
            List<Vector2> uvs = new();

            var levels = building.Levels != 0 ? building.Levels : 1;

            var height = levels * settings.LevelHeight;

            // Создание стен
            foreach (Vector3 pos in initialVertices)
            {
                // Каждый узел превращается в две вершины меша - на земле и на высоте здания
                vertices.Add(pos);
                vertices.Add(new Vector3(pos.x, (float)height, pos.z));

                // Если добавлены только две вершины, то треугольники строить ещё нельзя
                if (vertices.Count == 2)
                {
                    uvs.Add(new Vector2(0, 0) * uvScale);
                    uvs.Add(new Vector2(0, (float)height) * uvScale);

                    continue;
                }

                // Индекс последенй добавленной вершины
                int last = vertices.Count - 1;

                // Добавляем два треугольника, составляющие прямоугольник - грань между предыдущим и текущим узлом здания
                triangles.AddRange(new int[]
                {
                    // Судя по порядку обхода вершин, должен работать этот вариант, но, почему-то, работает другой
                    // По внешнему виду - порядок обратный предполагаемому. Хотя всё равно не понятно почему
                    //last - 3, last - 2, last - 1,
                    //last - 1, last - 2, last,

                    last - 1, last - 2, last - 3,
                    last, last - 2, last - 1,

                });

                // Расстояние между соседними вершинами на одном уровне
                var dist = (vertices[last - 3] - vertices[last - 1]).magnitude;
                Vector2 offset = new Vector2(dist, 0);

                // UV координаты для обеих вершин
                uvs.Add(uvs[uvs.Count - 2] + offset * uvScale);
                uvs.Add(uvs[uvs.Count - 2] + offset * uvScale);
            }

            // Создание объекта здания
            GameObject obj = new GameObject(
                $"Building[{building.Id}]", 
                typeof(MeshFilter), 
                typeof(MeshRenderer),
                typeof(MeshCollider),
                typeof(GeoData)
            );

            obj.GetComponent<GeoData>().Coordinate = building.Coordinate();
            obj.GetComponent<GeoData>().Tags["Building"] = building.ToString();

            // Создание и назначение меша
            Mesh mesh = new Mesh() { 
                vertices = vertices.ToArray(), 
                triangles = triangles.ToArray(),
                uv = uvs.ToArray()
            };

            // Пересчёт нормалей и тангенсов для освещения
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();

            obj.GetComponent<MeshFilter>().mesh = mesh;
            obj.GetComponent<MeshCollider>().sharedMesh = mesh;

            // Назначение материала, если указан
            if (BuildingMaterial != null)
                obj.GetComponent<MeshRenderer>().material = BuildingMaterial;

            // Крыша здания создаётся отдельно, чтобы у неё были собственные UV и текстура
            GameObject roof = CreateBuildingRoof(
                initialVertices.Select(v => v + new Vector3(0, (float)height, 0)).ToList()
            );

            roof.transform.parent = obj.transform;
            roof.transform.localPosition = Vector3.zero;
            //*/

            return obj;
        }

        // Создание крыши здания
        private GameObject CreateBuildingRoof(List<Vector3> vertices)
        {
            // Подготовка данных меша
            vertices = vertices.SkipLast().ToList();

            List<int> triangles = GeometryFunctions.Triangulate(vertices);

            List<Vector2> uvs = new();

            foreach (var vert in vertices)
                uvs.Add(new Vector2(vert.x, vert.z) / 10);

            // Создание объекта с необходимыми компонентами
            GameObject obj = new GameObject(
                "BuildingRoof",
                typeof(MeshFilter),
                typeof(MeshRenderer),
                typeof(MeshCollider)
            );

            // Создание и назначение меша
            Mesh mesh = new Mesh()
            {
                vertices = vertices.ToArray(),
                triangles = triangles.ToArray(),
                uv = uvs.ToArray()
            };

            mesh.RecalculateNormals();
            mesh.RecalculateTangents();

            obj.GetComponent<MeshFilter>().mesh = mesh;
            obj.GetComponent<MeshCollider>().sharedMesh = mesh;

            // Назначение материала, если указан
            if (BuildingMaterial != null)
                obj.GetComponent<MeshRenderer>().material = RoofMaterial;

            return obj;
        }

        // Создание пола чанка
        private GameObject CreateChunkFloor (double lat, double lon)
        {
            // Точка отсчёта - юго-восточный угол чанка
            GeoCoordinate origin = new GeoCoordinate(lat, lon);

            // Вершины
            List<Vector3> vertices = new()
            {
                GetUnityPosition(origin, origin),
                GetUnityPosition(origin.Add(latitude: settings.ChunkSize), origin),
                GetUnityPosition(origin.Add(longitude: settings.ChunkSize), origin),
                GetUnityPosition(origin.Add(settings.ChunkSize, settings.ChunkSize), origin)
            };

            List<Vector2> uvs = new();

            foreach (var vert in vertices)
                uvs.Add(new Vector2(vert.x, vert.z) / 10);
            

            // Треугольники
            List<int> trinagles = new()
            {
                0, 1, 2, 2, 1, 3
            };

            // Создание объекта
            GameObject obj = new GameObject(
                $"ChunkFloor", 
                typeof(MeshFilter), 
                typeof(MeshRenderer),
                typeof(MeshCollider)
            );

            // Создание и назначение меша
            Mesh mesh = new Mesh() {
                vertices = vertices.ToArray(),
                triangles = trinagles.ToArray(),
                uv = uvs.ToArray()
            };

            mesh.RecalculateNormals();
            mesh.RecalculateTangents();

            obj.GetComponent<MeshFilter>().mesh = mesh;
            obj.GetComponent<MeshCollider>().sharedMesh = mesh;

            // Назначение материала, если указан
            if (GroundMaterial != null)
                obj.GetComponent<MeshRenderer>().material = GroundMaterial;

            return obj;
        }

        // Определить координаты точки в системе координат unity используя вторую точку в качестве точки отсчёта
        public Vector3 GetUnityPosition (GeoCoordinate point, GeoCoordinate reference)
        {
            // Для отладки
            if (point == null)
                throw new NullReferenceException("Point must not be null");

            if (reference == null)
                throw new NullReferenceException("Reference must not be null");

            double x, z;

            GeoCoordinate referenceLat = reference.WithLatitude(point.Latitude);
            
            z = reference.GetDistanceTo(referenceLat) * Math.Sign(point.Latitude - reference.Latitude);
            x = referenceLat.GetDistanceTo(point) * Math.Sign(point.Longitude - reference.Longitude);
            
            return new Vector3((float)x, 0, (float)z);
        }

        // Получить координату чанка в пространстве Unity
        public Vector3 GetChunkPosition (GameObject chunk, GeoCoordinate reference)
        {
            var position =  GetUnityPosition(chunk.GetComponent<GeoData>().Coordinate, reference);

            // Тэги для отладки
            chunk.GetComponent<GeoData>().Tags["Coordinate"] = chunk.GetComponent<GeoData>().Coordinate.ToString();
            chunk.GetComponent<GeoData>().Tags["UnityPosition"] = position.ToString();

            return position;
        }

        // Перезагрузить материалы объектов
        public IEnumerator ReloadMaterials()
        {
            foreach (var (_, chunk) in LoadedChunks)
            {
                foreach (Transform t in chunk.transform)
                {
                    if (t.gameObject.name.Contains("Building"))
                    {
                        t.GetComponent<MeshRenderer>().material = BuildingMaterial;
                        t.Find("BuildingRoof").GetComponent<MeshRenderer>().material = RoofMaterial;
                    }
                    else if (t.gameObject.name.Contains("Floor"))
                    {
                        t.GetComponent<MeshRenderer>().material = GroundMaterial;

                        if (chunk.name.Contains("Empty"))
                        {
                            t.GetComponent<MeshRenderer>().material.color = EmptyColor;
                        }
                    }
                }

                yield return null;
            }
        }
    }
}
