﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Временное хранилище данных между сценами.
 */


using System;
using System.Collections.Generic;

// Временное ранилище данных между сценами
public static class PersistentState
{
    private static readonly Dictionary<string, object> data = new Dictionary<string, object>();

    public static bool Contains(string key) => data.ContainsKey(key);

    public static T Get<T>(string key, T default_ = default)
    {
        if (data.ContainsKey(key))
        {
            object value = data[key];

            if (value is not T)
                throw new ArgumentException("Invalid type");

            return (T)value;
        }

        return default_;
    }

    public static void Set<T>(string key, T value)
    {
        data[key] = value;
    }
}
